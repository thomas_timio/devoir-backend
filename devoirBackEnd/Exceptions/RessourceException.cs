﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace devoirBackEnd.Exceptions
{
    public class RessourceException : Exception
    {
        public int Statut { get; set; }

        public RessourceException(int statut, string message) : base(message)
        {
            this.Statut = statut;
        }
    }
}
