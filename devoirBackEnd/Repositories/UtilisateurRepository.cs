﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace devoirBackEnd.Repositories
{
    using Models;
    public interface UtilisateurRepository
    {
        public Utilisateur EnregistrerUn(Utilisateur utilisateur);
        public Utilisateur ModifierUtilisateur(int id, Utilisateur utilisateur);
        public void SupprimerUtilisateur(int id);
        public IEnumerable<Utilisateur> TrouverTous();
        public Utilisateur TrouverParId(int id);
        public IEnumerable<Utilisateur> TrouverTous(string nom);
        public IEnumerable<Utilisateur> TrouverTous(int age);
    }
}
