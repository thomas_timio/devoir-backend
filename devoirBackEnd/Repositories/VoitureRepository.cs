﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace devoirBackEnd.Repositories
{
    using Models;
    public interface VoitureRepository
    {

        public Voiture EnregistrerUne(Voiture voiture);

        public Voiture ModifierVoiture(Voiture voiture);

        public void SupprimerVoiture(int id);

        public IEnumerable<Voiture> TrouverToutes();
        public Voiture TrouverParId(int id);
        public IEnumerable<Voiture> TrouverParNom(string nom);
        public IEnumerable<Voiture> TrouverParMarque(string marque);
        public Voiture TrouverParImmatriculation(string immatriculation);
        public IEnumerable<Voiture> TrouverParProprietaire(int id);

    }
}
