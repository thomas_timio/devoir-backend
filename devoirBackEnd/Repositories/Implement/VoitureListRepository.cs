﻿using devoirBackEnd.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace devoirBackEnd.Repositories.Implement
{
    public class VoitureListRepository : VoitureRepository
    {
        private List<Voiture> voitures = new List<Voiture>();
        
        public Voiture EnregistrerUne(Voiture voiture)
        {
            voiture.Id = voitures.Count();
            voitures.Add(voiture);
            return voiture;
        }

        public Voiture ModifierVoiture(Voiture voiture)
        {
            this.voitures[voiture.Id] = voiture;
            return voiture;
        }

        public void SupprimerVoiture(int id)
        {
            this.voitures[id] = null;
        }

        public Voiture TrouverParId(int id)
        {
            return this.voitures[id];
        }

        public Voiture TrouverParImmatriculation(string immatriculation)
        {
            List<Voiture> voit = this.voitures.Where(v => v.Immatriculation == immatriculation).ToList();
            return voit.Count() != 0 ? voit[0] : null;
        }

        public IEnumerable<Voiture> TrouverParMarque(string marque)
        {
            List<Voiture> voit = this.voitures.Where(v => v.Marque == marque).ToList();
            return voit.Count() != 0 ? voit : null;
        }

        public IEnumerable<Voiture> TrouverParNom(string nom)
        {
            List<Voiture> voit = this.voitures.Where(v => v.Nom == nom).ToList();
            return voit.Count() != 0 ? voit : null;
        }

        public IEnumerable<Voiture> TrouverParProprietaire(int id)
        {
            IEnumerable<Voiture> voitures = TrouverToutes();
            List<Voiture> voit = new List<Voiture>();
            foreach (var v in voitures)
            {
                if (v.Proprietaire.Id == id)
                {
                    voit.Add(v);
                }
            }
            return voit;
        }

        public IEnumerable<Voiture> TrouverToutes()
        {
            return this.voitures;
        }
    }
}
