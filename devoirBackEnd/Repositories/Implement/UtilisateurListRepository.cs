﻿using devoirBackEnd.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace devoirBackEnd.Repositories.Implement
{
    public class UtilisateurListRepository : UtilisateurRepository
    {
        private List<Utilisateur> utilisateurs = new List<Utilisateur>();
        public Utilisateur EnregistrerUn(Utilisateur utilisateur)
        {
            utilisateur.Id = this.utilisateurs.Count();
            this.utilisateurs.Add(utilisateur);
            return utilisateur;
        }

        public Utilisateur ModifierUtilisateur(int id, Utilisateur utilisateur)
        {
            this.utilisateurs[id] = utilisateur;
            utilisateur.Id = id;
            return utilisateur;
        }

        public void SupprimerUtilisateur(int id)
        {
            this.utilisateurs[id] = null;
        }

        public Utilisateur TrouverParId(int id)
        {
            return this.utilisateurs[id];
        }

        public IEnumerable<Utilisateur> TrouverTous()
        {
            return this.utilisateurs.Where(util => util != null);
        }

        public IEnumerable<Utilisateur> TrouverTous(string nom)
        {
            return this.utilisateurs.Where(util => util.Nom == nom);
        }

        public IEnumerable<Utilisateur> TrouverTous(int age)
        {
            return this.utilisateurs.Where(util => util.Age == age);
        }
    }
}
