﻿using System;
using System.Collections.Generic;

namespace devoirBackEnd.DAO
{
    public partial class Voiture
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Marque { get; set; }
        public string Immatriculation { get; set; }
        public int? Proprietaire { get; set; }

        public virtual Utilisateur ProprietaireNavigation { get; set; }
    }
}
