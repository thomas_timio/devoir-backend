﻿using System;
using System.Collections.Generic;

namespace devoirBackEnd.DAO
{
    public partial class Utilisateur
    {
        public Utilisateur()
        {
            Voiture = new HashSet<Voiture>();
        }

        public int Id { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public int Age { get; set; }

        public virtual ICollection<Voiture> Voiture { get; set; }
    }
}
