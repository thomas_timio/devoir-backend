﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace devoirBackEnd.DAO
{
    public partial class devoir_backendContext : DbContext
    {
        public devoir_backendContext()
        {
        }

        public devoir_backendContext(DbContextOptions<devoir_backendContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Utilisateur> Utilisateur { get; set; }
        public virtual DbSet<Voiture> Voiture { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseMySql("server=localhost;port=3306;user=root;password=root;database=devoir_backend", x => x.ServerVersion("8.0.19-mysql"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Utilisateur>(entity =>
            {
                entity.ToTable("utilisateur");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Age).HasColumnName("age");

                entity.Property(e => e.Nom)
                    .IsRequired()
                    .HasColumnName("nom")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Prenom)
                    .IsRequired()
                    .HasColumnName("prenom")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");
            });

            modelBuilder.Entity<Voiture>(entity =>
            {
                entity.ToTable("voiture");

                entity.HasIndex(e => e.Proprietaire)
                    .HasName("fk_voiture_utilisateur_idx");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Immatriculation)
                    .IsRequired()
                    .HasColumnName("immatriculation")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Marque)
                    .IsRequired()
                    .HasColumnName("marque")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Nom)
                    .IsRequired()
                    .HasColumnName("nom")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Proprietaire).HasColumnName("proprietaire");

                entity.HasOne(d => d.ProprietaireNavigation)
                    .WithMany(p => p.Voiture)
                    .HasForeignKey(d => d.Proprietaire)
                    .HasConstraintName("fk_voiture_utilisateur");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
