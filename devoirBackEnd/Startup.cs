using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using devoirBackEnd.Repositories;
using devoirBackEnd.Repositories.Implement;
using devoirBackEnd.Services;
using devoirBackEnd.Services.Implement;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace devoirBackEnd
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(typeof(VoitureService), typeof(VoitureBackendService));
            services.AddSingleton(typeof(UtilisateurService), typeof(UtilisateurBackendService));
            services.AddSingleton(typeof(UtilisateurRepository), typeof(UtilisateurListRepository));
            services.AddSingleton(typeof(VoitureRepository), typeof(VoitureListRepository));
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
