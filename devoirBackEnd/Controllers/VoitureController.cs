﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace devoirBackEnd.Controllers
{
    using devoirBackEnd.Exceptions;
    using devoirBackEnd.Services;
    using Models;

    [Route("voitures")]
    [ApiController]
    public class VoitureController : ControllerBase
    {
        private VoitureService service;

        public VoitureController(VoitureService service)
        {
            this.service = service;
        }

        [HttpPost]
        [Route("")]
        public IActionResult EnregistrerUne([FromBody] Voiture voiture)
        {
 
                return Ok(this.service.EnregistrerUne(voiture));
        }

        [HttpGet]
        [Route("")]
        public IActionResult TrouverToutes()
        {
            return Ok(this.service.TrouverToutes());
        }

        [HttpPut]
        [Route("")]
        public IActionResult ModifierVoiture([FromBody] Voiture voiture)
        {
            try
            {
                return Ok(this.service.ModifierVoiture(voiture));
            }
            catch (RessourceException e)
            {
                if (e.Statut == 404)
                    return NotFound(e.Message);
                else
                {
                    return BadRequest(e.Message);
                }
            }
        }

        [HttpPut]
        [Route("{id}/proprietaire/{idUser}")]
        public IActionResult ModifierProprietaire(int id, int idUser)
        {
            try
            {
                return Ok(this.service.ModifierProprietaire(id, idUser));
            }
            catch (RessourceException e)
            {
                if (e.Statut == 404)
                    return NotFound(e.Message);
                else
                {
                    return BadRequest(e.Message);
                }
            }
        }


        [HttpDelete]
        [Route("{id}")]
        public IActionResult SupprimerVoiture(int id)
        {
            try
            {
                this.service.SupprimerVoiture(id);
                return Ok();
            }
            catch (RessourceException e)
            {
                if (e.Statut == 404)
                    return NotFound(e.Message);
                else
                {
                    return BadRequest(e.Message);
                }
            }
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult TrouverParId(int id)
        {
            try
            {
                return Ok(this.service.TrouverParId(id));
            }
            catch (RessourceException e)
            {
                if (e.Statut == 404)
                    return NotFound(e.Message);
                else
                {
                    return BadRequest(e.Message);
                }
            }
        }

        [HttpGet]
        [Route("immatriculation/{immatriculation}")]
        public IActionResult TrouverParImmatriculation(string immatriculation)
        {
            try
            {
                return Ok(this.service.TrouverParImmatriculation(immatriculation));
            }
            catch (RessourceException e)
            {
                if (e.Statut == 404)
                    return NotFound(e.Message);
                else
                {
                    return BadRequest(e.Message);
                }
            }
        }

        [HttpGet]
        [Route("marque/{marque}")]
        public IActionResult TrouverParMarque(string marque)
        { 
            try
            {
                return Ok(this.service.TrouverParMarque(marque));
            }
            catch (RessourceException e)
            {
                if (e.Statut == 404)
                    return NotFound(e.Message);
                else
                {
                    return BadRequest(e.Message);
                }
            }
        }

        [HttpGet]
        [Route("nom/{nom}")]
        public IActionResult TrouverParNom(string nom)
        {
            try
            {
                return Ok(this.service.TrouverParNom(nom));
            }
            catch (RessourceException e)
            {
                if (e.Statut == 404)
                    return NotFound(e.Message);
                else
                {
                    return BadRequest(e.Message);
                }
            }
        }

        [HttpGet]
        [Route("utilisateur/{id}")]
        public IActionResult TrouverParProprietaire(int id)
        {
            try
            {
                return Ok(this.service.TrouverParProprietaire(id));
            }
            catch (RessourceException e)
            {
                if (e.Statut == 404)
                    return NotFound(e.Message);
                else
                {
                    return BadRequest(e.Message);
                }
            }
        }
    }
}