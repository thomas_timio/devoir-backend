﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using devoirBackEnd.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace devoirBackEnd.Controllers
{
    using devoirBackEnd.Exceptions;
    using Models;

    [Route("utilisateurs")]
    [ApiController]
    public class UtilisateurController : ControllerBase
    {
        private UtilisateurService service;

        public UtilisateurController(UtilisateurService service)
        {
            this.service = service;
        }

        [HttpPost]
        [Route("")]
        public IActionResult EnregistrerUn([FromBody] Utilisateur utilisateur)
        {
            try
            {
                return Ok(this.service.EnregistrerUn(utilisateur));
            }
            catch (RessourceException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("")]
        public IActionResult TrouverTous()
        {
            return Ok(this.service.TrouverTous());
        }

        [HttpPut]
        [Route("{id}")]
        public IActionResult ModifierUtilisateur(int id, [FromBody] Utilisateur utilisateur)
        {
            try
            {
                return Ok(this.service.ModifierUtilisateur(id, utilisateur));
            }
            catch (RessourceException e)
            {
                if (e.Statut == 404)
                    return NotFound(e.Message);
                else
                {
                    return BadRequest(e.Message);
                }
            }
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult SupprimerUtilisateur(int id)
        {
            try
            {
                this.service.SupprimerUtilisateur(id);
                return Ok();
            }
            catch (RessourceException e)
            {
                if (e.Statut == 404)
                    return NotFound(e.Message);
                else
                {
                    return BadRequest(e.Message);
                }
            }
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult TrouverParId(int id)
        {
            return Ok(this.service.TrouverParId(id));
        }

        [HttpGet]
        [Route("nom/{nom}")]
        public IActionResult TrouverTous(string nom)
        {
            return Ok(this.service.TrouverTous(nom));
        }

        [HttpGet]
        [Route("age/{age}")]
        public IActionResult TrouverTous(int age)
        {
            return Ok(this.service.TrouverTous(age));
        }

    }
}