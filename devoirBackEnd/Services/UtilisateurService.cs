﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace devoirBackEnd.Services
{
    using Models;

    public interface UtilisateurService
    {
        /// <summary>
        /// Permet d'ajouter un utilisateur à la liste
        /// </summary>
        /// <param name="utilisateur"></param>
        /// <returns></returns>
        public Utilisateur EnregistrerUn(Utilisateur utilisateur);

        /// <summary>
        /// Permet de modifier l'utilisateur; pour une modification des voitures possédées, utiliser
        /// ModifierPropriétaire, AjouterVoiture, SupprimerVoiture
        /// </summary>
        /// <param name="id"></param>
        /// <param name="utilisateur"></param>
        /// <returns></returns>
        public Utilisateur ModifierUtilisateur(int id, Utilisateur utilisateur);

        /// <summary>
        /// permet de supprimer un utilisateur de la liste 
        /// </summary>
        /// <param name="id"></param>
        public void SupprimerUtilisateur(int id);

        /// <summary>
        /// Retourne l'ensemble des utilisateurs
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Utilisateur> TrouverTous();

        /// <summary>
        /// Permet de trouver un utilisateur avec son Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Utilisateur TrouverParId(int id);

        /// <summary>
        /// Retourne tous les utilisateurs portant un nom donné.
        /// </summary>
        /// <param name="nom"></param>
        /// <returns></returns>
        public IEnumerable<Utilisateur> TrouverTous(string nom);

        /// <summary>
        /// Retourne les utilisateurs ayant un âge donné
        /// </summary>
        /// <param name="age"></param>
        /// <returns></returns>
        public IEnumerable<Utilisateur> TrouverTous(int age);

    }
}
