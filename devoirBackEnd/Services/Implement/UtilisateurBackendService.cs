﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace devoirBackEnd.Services.Implement
{
    using devoirBackEnd.Exceptions;
    using devoirBackEnd.Repositories;
    using Models;
    public class UtilisateurBackendService : UtilisateurService
    {
        private static List<Utilisateur> utilisateurs = new List<Utilisateur>();
        private UtilisateurRepository repo;


        public UtilisateurBackendService(UtilisateurRepository repo)
        {
            this.repo = repo;
        }

        public Utilisateur EnregistrerUn(Utilisateur utilisateur)
        {
            return this.repo.EnregistrerUn(utilisateur);
        }

        public Utilisateur ModifierUtilisateur(int id, Utilisateur utilisateur)
        {
            if (id < utilisateurs.Count() && id >= 0 && this.repo.TrouverParId(id) != null)
            {
                return this.repo.ModifierUtilisateur(id, utilisateur);
            }
            else if (this.repo.TrouverParId(id) == null)
            {
                throw new RessourceException(404, "Cet utilisateur n'existe pas");
            }
            else
            {
                throw new RessourceException(400, "Erreur indéfinie");
            }
        }

        public void SupprimerUtilisateur(int id)
        {

            if (id < utilisateurs.Count() && id >= 0 && this.repo.TrouverParId(id) != null)
            {
                this.repo.SupprimerUtilisateur(id);
            }
            else if (this.repo.TrouverParId(id) == null)
            {
                throw new RessourceException(404, "Cet utilisateur n'existe pas");
            }
        }

        public IEnumerable<Utilisateur> TrouverTous()
        {
            return this.repo.TrouverTous();
        }

        public Utilisateur TrouverParId(int id)
        {
            if (this.repo.TrouverParId(id) != null)
            {
                return this.repo.TrouverParId(id);
            }
            else if (this.repo.TrouverParId(id) == null)
            {
                throw new RessourceException(404, "Cet utilisateur n'existe pas");
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<Utilisateur> TrouverTous(string nom)
        {
            return this.repo.TrouverTous(nom);
        }

        public IEnumerable<Utilisateur> TrouverTous(int age)
        {
            return this.repo.TrouverTous(age);
        }


    }
}
