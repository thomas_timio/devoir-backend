﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace devoirBackEnd.Services.Implement
{
    using Repositories;
    using Models;
    using devoirBackEnd.Exceptions;

    public class VoitureBackendService : VoitureService
    {
        private static List<Voiture> voitures = new List<Voiture>();
        private VoitureRepository repo;
        private UtilisateurService userServ;

        public VoitureBackendService(VoitureRepository repo, UtilisateurService userServ)
        {
            this.repo = repo;
            this.userServ = userServ;
        }

        public Voiture EnregistrerUne(Voiture voiture)
        {
            Voiture v = voiture;
            Utilisateur u = this.userServ.TrouverParId(v.Proprietaire.Id);
            v.Proprietaire = u;
            return u.Age >= 18 ? this.repo.EnregistrerUne(v) : null; // Si le propriétaire est majeur, on l'autorise à ajouter la voiture.
        }

        public Voiture ModifierProprietaire(int id, int idUser)
        {
            voitures = RemplirListe();
            if (id < voitures.Count() && id >= 0 && this.userServ.TrouverParId(idUser) != null)
            {
            voitures[id].Proprietaire = this.userServ.TrouverParId(idUser);
            
                return this.repo.ModifierVoiture(voitures[id]);
            }
            else if (id >= voitures.Count())
            {
                throw new RessourceException(404, "Cette voiture n'existe pas");
            }
            else if (this.userServ.TrouverParId(idUser) == null)
            {
                throw new RessourceException(404, "Cet utilisateur n'existe pas");
            }
            else
            {
                throw new RessourceException(404, "Erreur indéfinie");
            }
        }

        public Voiture ModifierVoiture(Voiture voiture)
        {
            voitures = RemplirListe();
            if (voiture.Id < voitures.Count())
            {
                return this.repo.ModifierVoiture(voiture);
            }
            else
            {
                throw new RessourceException(404, "La voiture n'existe pas");
            }
        }

        public void SupprimerVoiture(int id)
        {
            voitures = RemplirListe();
            if(id < voitures.Count())
            {
                this.repo.SupprimerVoiture(id);
            }
            else
            {
                throw new RessourceException(404, "La voiture n'existe pas");
            }
        }

        public Voiture TrouverParId(int id)
        {
            voitures = RemplirListe();
            if (id < voitures.Count())
            {
                return this.repo.TrouverParId(id);
            }
            else
            {
                throw new RessourceException(404, "La voiture n'existe pas");
            }
        }

        public Voiture TrouverParImmatriculation(string immatriculation)
        {
            var result = this.repo.TrouverParImmatriculation(immatriculation);
            if (result != null)
            {
                return result;
            }
            else
            {
                throw new RessourceException(404, "La voiture n'existe pas");
            }
        }

        public IEnumerable<Voiture> TrouverParMarque(string marque)
        {
            var result = this.repo.TrouverParMarque(marque);
            if (result != null)
            {
                return result;
            }
            else
            {
                throw new RessourceException(404, "La voiture n'existe pas");
            }
        }

        public IEnumerable<Voiture> TrouverParNom(string nom)
        {
            var result = this.repo.TrouverParNom(nom);
            if (result != null)
            {
                return result;
            }
            else
            {
                throw new RessourceException(404, "La voiture n'existe pas");
            }
        }

        public IEnumerable<Voiture> TrouverToutes()
        {
            return this.repo.TrouverToutes();
        }

        public IEnumerable<Voiture> TrouverParProprietaire(int id)
        {
            var result = this.repo.TrouverParProprietaire(id);
            if (result != null)
            {
                return result;
            }
            else
            {
                throw new RessourceException(404, "La voiture n'existe pas");
            }
        }

        /// <summary>
        /// Permet de remplir la liste pour les exceptions ; pas obligatoire, simplification.
        /// </summary>
        /// <returns></returns>
        public List<Voiture> RemplirListe()
        {
            return TrouverToutes().ToList();
        }
    }
}
