﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace devoirBackEnd.Services
{
    using Models;

    public interface VoitureService
    {
        /// <summary>
        /// Enregistrement d'une nouvelle voiture. Elle doit avoir un propriétaire dans sa définition.
        /// </summary>
        /// <param name="voiture"></param>
        /// <returns></returns>
        public Voiture EnregistrerUne(Voiture voiture);

        /// <summary>
        /// modifier la voiture permettra de modifier les caractéristiques de la voiture. Pour le changement de propriétaire, se référer à ModifierProprietaire.
        /// </summary>
        /// <param name="voiture"></param>
        /// <returns></returns>
        public Voiture ModifierVoiture(Voiture voiture);

        /// <summary>
        /// supprimer une voiture de la liste grâce à son Id
        /// </summary>
        /// <param name="id"></param>
        public void SupprimerVoiture(int id);

        /// <summary>
        /// Permet de modifier le propriétaire de la voiture en en attribuant un nouveau.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="idUser">L'utilisateur doit déjà être présent dans la liste des utilisateurs</param>
        /// <returns></returns>
        public Voiture ModifierProprietaire(int id, int idUser);

        /// <summary>
        /// retourne toutes les occurences voiture
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Voiture> TrouverToutes();

        /// <summary>
        /// permet de retrouver la voiture correspondant à un id précis
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Voiture TrouverParId(int id);

        /// <summary>
        /// Retrouver une liste de voitures portant un nom précis
        /// </summary>
        /// <param name="nom"></param>
        /// <returns></returns>
        public IEnumerable<Voiture> TrouverParNom(string nom);

        /// <summary>
        /// Retrouver une liste de voitures d'une marque précise
        /// </summary>
        /// <param name="marque"></param>
        /// <returns></returns>
        public IEnumerable<Voiture> TrouverParMarque(string marque);

        /// <summary>
        /// Retourne une unique occurence d'une voiture portant une imamtriculation.
        /// </summary>
        /// <param name="immatriculation"></param>
        /// <returns></returns>
        public Voiture TrouverParImmatriculation(string immatriculation);

        /// <summary>
        /// Retourne une liste de voitures appartenant à un utilisateur défini.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<Voiture> TrouverParProprietaire(int id);
    }
}
