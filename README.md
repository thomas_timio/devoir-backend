# Devoir Aston 
#### A rendre pour le 22/03/2020

Ce devoir a pour but de s'entrainer sur la technologie ASP.NET et MVC.NET. Il n'a aucune autre vocation.

## Consignes

- Il consiste en deux CRUD de voiture et personne.
- Une voiture possède:
    - un nom
    - une marque
    - une imatriculation
    - un proprietaire
- Un utilisateur possède:
    - un nom
    - un prenom
    - un age
- Il y aura deux Controllers (VoitureController et PersonneController)
- Il y aura deux services.
- Il y aura deux repository
- Ils sont reliés par IOD (injection de dependance)
- Le controleur peut etre un bouchon (utilisation d'une liste)

## En Bonus:

- Connecter a une Base de donnée
- Changer le Repository pour qu'il lance les requetes grace a Entity
- N'ajouter la voiture uniquement si le propriétaire est majeur.
- Faire le retour des status et la gestion des erreurs.
- Documenter votre code

##### Devoir créé par Killian Raoux pour Semifir, formation Aston Ecole.